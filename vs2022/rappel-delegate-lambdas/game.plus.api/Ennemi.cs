﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.plus.api
{
	public class Ennemi
	{
		private static Random _random = new Random();

        public string Prenom { get; set; }

        public Ennemi(string prenom)
        {
            this.Prenom = prenom;
        }

        public void SeDeplacer(SePlacerDansUnLieu sePlacer)
		{


			sePlacer( new Position(_random.Next(0, 100), _random.Next(0, 100)) );
		}
	}
}
