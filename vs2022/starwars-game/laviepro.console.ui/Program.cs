﻿
using laviedesdevs;
using laviepro.console.ui;
using lavieprofessionnelle;

var velo = new Velo();

// velo.Vitesse = 50;

var trotinettePasAParis = new Trotinette();
var salarie = new Salarie(trotinettePasAParis);

salarie.ArriveTravail += SalarieArriveTravail;
salarie.ArriveTravail += (salarie, arguments) =>
{
    Console.WriteLine("Je suis arrivé au travail ! {0}", salarie.Name);
};


salarie.ArriveTravail -= SalarieArriveTravail;

void SalarieArriveTravail(Salarie arg1, EventArgs arg2)
{
	Console.WriteLine("Je suis arrivé au travail ! {0}", arg1.Name);
}

salarie.SeDeplacerAuTravail();


//---------------------------------------
IDeveloppeur developper = new Salarie();




void CreerDuCode(List<IDeveloppeur> developpeurs)
{
	developpeurs.ForEach(developper =>
	{
		developper.EcrireDuCode();
		developper.AllerALaMachineACafe();
	});
}

CreerDuCode(new()
{
	developper,
	new CestPasBien()
});