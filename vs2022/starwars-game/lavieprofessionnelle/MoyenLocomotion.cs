﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lavieprofessionnelle
{
    public abstract class MoyenLocomotion
    {
        public MoyenLocomotion(int vitesse)
        {
            this.Vitesse = vitesse;
        }

        // public int Vitesse { get; private set; } = 10;
        public int Vitesse { get; init; } = 10;
    }
}
