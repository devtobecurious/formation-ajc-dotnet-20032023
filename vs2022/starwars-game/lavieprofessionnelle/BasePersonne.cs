﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lavieprofessionnelle
{
	public abstract class BasePersonne
	{
		public string Name { get; set; }

		public abstract void Manger();
	}
}
