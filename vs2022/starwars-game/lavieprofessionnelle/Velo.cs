﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lavieprofessionnelle
{
	public class Velo : MoyenLocomotion, IMoyenTransport
    {
        public Velo() : this("")
        {
        }

		public Velo(string marque) : this(10, "")
		{
			
		}

        public Velo(int vitesse): this(vitesse, "")
        {
			
        }

        //public Velo(bool bouleEtBill) : base(10)
        //{

        //}

        public Velo(int vitesse, string marque) : base(vitesse)
        {
            if (string.IsNullOrEmpty(marque))
            {
                marque = "Valeur par défaut";
            }
            this.Marque = marque;
        }

        public string Marque { get; set; }

//		public int Vitesse { get; set; } = 10;

        public void Rouler()
		{
			throw new NotImplementedException();
		}
	}
}
