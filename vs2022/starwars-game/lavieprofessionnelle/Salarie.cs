﻿using laviedesdevs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lavieprofessionnelle
{
	public class Salarie : BasePersonne, IDeveloppeur
	{
		// public Velo velo = new Velo(); à éviter 
		private IMoyenTransport moyenDeplacement;
		public event Action<Salarie, EventArgs> ArriveTravail;

        public Salarie()
        {
        }

        public Salarie(IMoyenTransport moyen)
        {
			this.moyenDeplacement = moyen;
        }

        public void SeDeplacerAuTravail()
		{
			this.moyenDeplacement.Rouler();
			this.ArriveTravail?.Invoke(this, EventArgs.Empty);
		}

		public void EcrireDuCode()
		{
			throw new NotImplementedException();
		}

		public void AllerAuxToilettes()
		{
			throw new NotImplementedException();
		}

		public void AllerALaMachineACafe()
		{
			throw new NotImplementedException();
		}

		public override void Manger()
		{
			throw new NotImplementedException();
		}
	}
}
