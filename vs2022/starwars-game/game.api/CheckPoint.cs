﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Sauvegarde de la partie à un instant donné
    /// </summary>
    public class CheckPoint
    {
        #region Constructors
        public CheckPoint(int gameId, int persoPointVie)
        {
            this.PersoPointsDeVie = persoPointVie;
            this.GameId = gameId;
        }
        #endregion

        #region Properties
        public int GameId { get; set; }

        public int PersoPointsDeVie { get; set; }
        #endregion
    }
}
